# Sample .bashrc for SuSE Linux
# Copyright (c) SuSE GmbH Nuernberg

# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit

# For some news readers it makes sense to specify the NEWSSERVER variable here
#export NEWSSERVER=your.news.server

# If you want to use a Palm device with Linux, uncomment the two lines below.
# For some (older) Palm Pilots, you might need to set a lower baud rate
# e.g. 57600 or 38400; lowest is 9600 (very slow!)
#
#export PILOTPORT=/dev/pilot
#export PILOTRATE=115200

get_cpu_temperature() {
    CEL=$'\xc2\xb0C'
    temp=$( cat /sys/devices/virtual/thermal/thermal_zone0/temp )
    temp=`expr $temp / 1000`
    echo $temp$CEL
}

alias bruhhhhh="neofetch"
alias ls="logo-ls"
alias neofetch="clear && neofetch"
alias vim="emacs -nw"
alias emacs="emacs -nw"
alias sl="ls"
alias free="flatpak run io.freetubeapp.FreeTube"
alias yt="ytfzf -t -T chafa -i kitty -e subscription-manager -e smart-thumb-download"
alias hypr="emacs /home/deadvey/.config/hypr/hyprland.conf"
alias wayc="emacs /home/deadvey/.config/waybar/config"
alias ways="emacs /home/deadvey/.config/waybar/style.css"
alias bashrc="emacs /home/deadvey/.bashrc"
alias startsh="emacs /home/deadvey/scripts/start.sh"

test -s ~/.alias && . ~/.alias || true

PS1="\n \w\n > "

fastfetch
